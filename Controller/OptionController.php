<?php

namespace App\Http\Controllers\Dashboard;

use App\Facades\OptionsService;
use App\Http\Requests\Dashboard\UpdateOption;
use App\Models\Option;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OptionController extends Controller
{
    public function edit()
    {
        $options = OptionsService::getSortedOptions();
        return view('dashboard.options.edit', compact('options'));
    }

    public function update(UpdateOption $request, Option $option)
    {
        $code = 200;

        foreach ($request->validated() as $key => $item) {
            $option->whereName($key)->update(['value' => $item]) || $code = 400;
        }

        return response()->json($request, $code);
    }
}