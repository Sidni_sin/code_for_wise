<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\Dashboard\StoreEmbedCode;
use App\Http\Requests\Dashboard\UpdateEmbedCode;
use App\Models\EmbedCode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmbedCodeController extends Controller
{
    public function index()
    {
        $embedCode = EmbedCode::all();
        return view('dashboard.embed_code.index', compact('embedCode'));
    }

    public function create()
    {
        return view('dashboard.embed_code.create');
    }

    public function store(StoreEmbedCode $request, EmbedCode $embedCode)
    {
        $embedCode->fill($request->validated());

        $embedCode->save();

        return redirect()->route('dashboard.store.embed-codes.index');

    }

    public function show($id)
    {
        //
    }

    public function edit(EmbedCode $embedCode)
    {
        return view('dashboard.embed_code.edit', compact('embedCode'));
    }

    public function update(UpdateEmbedCode $request, EmbedCode $embedCode)
    {
        $embedCode->fill($request->validated());
        $embedCode->save();

        return redirect()->route('dashboard.store.embed-codes.index');
    }

    public function destroy(EmbedCode $embedCode)
    {
        $embedCode->delete();

        return redirect()->back();
    }
}
