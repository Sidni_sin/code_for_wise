<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class MainController extends Controller
{
    public function index()
    {
//        $this->_epnApiAccess->AddRequestGetTopMonthly('top_sales_1', 'ru', 'RUR', 'sales');
//        $this->_epnApiAccess->RunRequests();
//
//        $offers = $this->_epnApiAccess->GetRequestResult('top_sales_1')['offers'];
//
//        $this->_epnApiAccess->AddRequestGetOfferInfo('one', $offers[0]['id']);
//        $this->_epnApiAccess->AddRequestGetOfferInfo('two', $offers[1]['id']);
//        $this->_epnApiAccess->AddRequestGetOfferInfo('three', $offers[2]['id']);
//        $this->_epnApiAccess->AddRequestGetOfferInfo('four', $offers[3]['id']);
//        $this->_epnApiAccess->AddRequestGetOfferInfo('five', $offers[4]['id']);
//
//        $this->_epnApiAccess->RunRequests();
//
//        dump($this->_epnApiAccess->GetRequestResult('one'));
//        dump($this->_epnApiAccess->GetRequestResult('two'));
//        dump($this->_epnApiAccess->GetRequestResult('three'));
//        dump($this->_epnApiAccess->GetRequestResult('four'));
//        dump($this->_epnApiAccess->GetRequestResult('five'));

//        dump(Auth::user()->email);

        return view('dashboard.main');
    }
}
