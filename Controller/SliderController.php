<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\Dashboard\StoreSlider;
use App\Http\Requests\Dashboard\UpdateSlider;
use App\Models\Slider;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SliderController extends Controller
{
    public function index()
    {
        $sliders = Slider::all();
        return view('dashboard.sliders.index', compact('sliders'));
    }

    public function create()
    {
        return view('dashboard.sliders.create');
    }

    public function store(StoreSlider $request, Slider $slider)
    {
        $image = $request->file('image');
        $background_image = $request->file('background_image');

        $slider->fill($request->validated());

        $slider->src = is_null($image) ? null : $slider->saveImage($image);
        $slider->background_src = $slider->saveImage($background_image);

        $slider->position = $slider->max('position') + 1;

        $slider->save();
        return redirect()->route('dashboard.store.sliders.index');
    }

    public function edit(Slider $slider)
    {
        $positions = Slider::all('position');
        return view('dashboard.sliders.edit', compact('slider', 'positions'));
    }

    public function update(UpdateSlider $request, Slider $slider)
    {
        $oldPosition = $slider->position;
        $slider->fill($request->validated());

        if(!is_null($request->image)){
            $slider->src = $slider->saveImage($request->file('image'));
        }

        if(!is_null($request->background_image)){
            $slider->background_src = $slider->saveImage($request->file('background_image'));
        }

        DB::transaction(function () use ($slider, $oldPosition){
            if($oldPosition != $slider->position){
                $oldSlider = Slider::where('position', $slider->position)->first();
                $oldSlider->position = (int)$oldPosition;

                $slider->save();
                $oldSlider->save();
            }else{
                $slider->save();
            }
        });

        return redirect()->back();
    }

    public function destroy(Slider $slider)
    {
        $slider->delete();
        return redirect()->back();
    }
}
