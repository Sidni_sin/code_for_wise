<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\Dashboard\UpdateComment;
use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{
    public function index()
    {
        $comments = Comment::orderBy('id', 'DESC')->get();

        return view('dashboard.comments.index', compact('comments'));
    }

    public function change(Comment $comment)
    {
        $comment->is_visible = (!$comment->is_visible);
        $comment->save();

        return response()->json($comment->only('is_visible'));
    }

    public function edit(Comment $comment)
    {
        return view('dashboard.comments.edit', compact('comment'));
    }

    public function update(UpdateComment $request, Comment $comment)
    {
        $comment->fill($request->validated());
        $comment->save();
        return redirect()->route('dashboard.store.comments.index');
    }

    public function destroy(Comment $comment)
    {
        $comment->delete();
        return redirect()->route('dashboard.store.comments.index');
    }


}
