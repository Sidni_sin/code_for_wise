<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ImageController extends Controller
{
    public function destroy (Image $image)
    {
        $image->delete();
        return response()->json($image, 200);
    }
}
