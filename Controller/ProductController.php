<?php

namespace App\Http\Controllers\Dashboard;

use App\Facades\SubDomainService;
use App\Http\Requests\Dashboard\StoreProduct;
use App\Http\Requests\Dashboard\UpdateProduct;
use App\Models\Category;
use App\Models\CategoryProduct;
use App\Models\Image;
use App\Models\Partners\Factory;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ProductController extends Controller
{

    public function __construct(Factory $factory)
    {
        $this->partner = $factory->create(SubDomainService::getProgram(), SubDomainService::getUserApiKey(), SubDomainService::getUserPrivateKey(), SubDomainService::getUserHash());
    }

    public function index()
    {
        $products = Product::orderBy('id', 'DESC')->get();
        return view('dashboard.products.index', compact('products'));
    }

    public function create(Category $category)
    {
        $categories = $category->all();

        $prepareTree = $category->preparationForTree($categories->toArray());

        $treeCats = null;

        if (!empty($prepareTree)){
            $treeCats = $category->createTreeHyphen($prepareTree,  min(array_keys($prepareTree)));
        }

        return view('dashboard.products.create', compact('treeCats'));
    }

    public function store(StoreProduct $request, Product $product)
    {
        $product->fill($request->validated());

        unset($product->categories);

        $categories = $request->validated()['categories'] ?? null;

        $product->p_id = (int)$product->p_id;
        $product->slug = str_slug($product->name);
        $product->short_description = $product->name;
        $product->specifications = '[]';
        $product->partner_link = '';
        $product->is_visible = false;

        while (Product::where(['slug' => $product->slug])->first()){
            $product->slug .=  uniqid('-');
        }

        DB::transaction(function () use ($product, $categories){

            $product->save();

            $catIds = [];
            if($categories){
                foreach ($categories as $c){
                    $catIds[] = ['category_id' => (int)$c];
                }
            }

            $product->categoryProduct()->createMany($catIds);

            $product->partner_link = $this->partner->getPartnerLink($product->url, $product->id);

            $product->save();
        });


        return response()->json($request->all(), 201);
    }

    public function edit(Product $product, Category $category)
    {
        $categories = Category::all();
        $tree = null;

        if (!$categories->isEmpty()){
            $prepareTree = $category->preparationForTree($categories->toArray());
            $tree = $category->createTreeHyphen($prepareTree,  min(array_keys($prepareTree)));
        }
        $product->load('categories');

        $selectIds = [];
        foreach ($product->categories as $c){
            $selectIds[] = $c->id;
        }

        return view('dashboard.products.edit', compact('product', 'tree', 'selectIds'));
    }

    public function update(UpdateProduct $request, Product $product, CategoryProduct $categoryProduct)
    {
        $product->fill($request->validated());
        $categories = $request->validated()['categories'] ?? null;

        $pictures = $request->file('pictures');
        $picture  = $request->file('picture');

        $pictures = !is_null($pictures) ? $product->saveImages($request->file('pictures')) : null;

        $picture  = !is_null($picture) ? $product->saveImage($request->file('picture')) : null;

        $product->picture = is_null($picture) ? $product->picture : $picture;

        $images_array = [];

        if($pictures){
            foreach ($pictures as $pic){
                $images_array[] = ['src' => $pic];
            }
        }

        DB::transaction(function () use ($product, $images_array, $categories, $categoryProduct){
            $product->save();

            if(!empty($images_array)) {
                $product->images()->createMany($images_array);
            }

            $product->categoryProduct()->delete();

            $catIds = [];
            if($categories){
                foreach ($categories as $c){
                    $catIds[] = ['category_id' => (int)$c];
                }
            }

            $product->categoryProduct()->createMany($catIds);
        });

        return redirect()->back();
    }

    public function destroy(Product $product)
    {
        DB::transaction(function () use ($product){
            $product->images()->delete();
            $product->categoryProduct()->delete();
            $product->delete();
        });

        return redirect()->back();
    }
}
