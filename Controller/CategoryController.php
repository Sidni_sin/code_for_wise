<?php

namespace App\Http\Controllers\Dashboard;

use App\Facades\SubDomainService;
use App\Http\Requests\Dashboard\StoreCategory;
use App\Http\Requests\Dashboard\UpdateCategory;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Validator;

class CategoryController extends Controller
{
    public function index(Category $category)
    {
        $categories = $category->all();

        $prepareTree = $category->preparationForTree($categories->toArray());

        $tree = null;
        $categoryModel = null;

        if (!empty($prepareTree)){
            $tree = $category->createTreeHyphen($prepareTree,  min(array_keys($prepareTree)));
            $categoryModel = $category;
        }

        return view('dashboard.categories.index', compact('categoryModel', 'tree'));
    }

    public function create(Category $category)
    {
        $categories = Category::all();

        $tree = null;
        $prepareTree = null;

        if(!$categories->isEmpty()){
            $prepareTree = $category->preparationForTree($categories->toArray());
            $tree = $category->createTreeHyphen($prepareTree,  min(array_keys($prepareTree)));
        }

        return view('dashboard.categories.create', compact('tree'));
    }

    public function store(StoreCategory $request, Category $category)
    {
        $category->fill($request->validated());

        $category->slug = str_slug($category->name);

        if (is_null($category->parent_id)){
            $category->parent_id = 0;
            $category->position  = 1 + ($category->max('position') ?? 0);
        }else{
            $category->position  = 1 + ($category->whereParentId($category->parent_id)->max('position') ?? 0);
        }

        $category->save();

        return redirect()->route('dashboard.store.categories.index');
    }

    public function edit(Category $category)
    {
        $categories = $category->where('id', '!=', $category->id)->where('parent_id', '!=', $category->id)->get();

        $tree = null;
        $prepareTree = null;

        if(!$categories->isEmpty()){
            $prepareTree = $category->preparationForTree($categories->toArray());
            $tree = $category->createTreeHyphen($prepareTree, min(array_keys($prepareTree)));
        }

        return view('dashboard.categories.edit', compact('category', 'tree'));
    }

    public function update(UpdateCategory $request, Category $category)
    {
        $valid = Validator::make($request->all(), [
            'parent_id'     => 'required',
        ]);

        $id = Category::where('slug', $request->segment(4))->first()->id;

        $valid->sometimes(
            ['parent_id'],
            Rule::exists('categories', 'id')->where(function ($query) use ($id) {
                return $query->where('store_id', SubDomainService::getStoreId())->where('parent_id', '!=', $id);
            }),
            function($input){
                return ($input->parent_id != 0) ? true : false;
            }
        );

        if($valid->fails()){
            return redirect()->back()
                ->withErrors($valid)
                ->withInput();
        }

        $category->fill($request->validated());
        $category->slug = str_slug($category->name);
        $category->parent_id = $request->parent_id;

        $category->save();

        return redirect()->route('dashboard.store.categories.index');
    }

    public function destroy(Category $category)
    {
        DB::transaction(function () use ($category){
            $category->where('parent_id', $category->id)->update(['parent_id' => $category->parent_id]);
            $category->delete();
        });

        return redirect()->route('dashboard.store.categories.index');
    }
}
