<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    CONST DESIGN_DEFAULT = 'default';

    protected $fillable = [
        'slug', 'name', 'user_api_key', 'user_hash', 'user_private_key',
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
