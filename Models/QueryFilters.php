<?php

namespace App\Models;

use \Illuminate\Database\Eloquent\Builder;
use InvalidArgumentException;
use phpDocumentor\Reflection\Types\Boolean;

class QueryFilters extends Builder
{
    public function filterWhere($keyRequest, $nameColumn, $operator = '=')
    {
        if (request($keyRequest)) {
            $this->query->where($nameColumn, $operator, request($keyRequest));
        }
        return $this;
    }

    public function filterOrderBy($keyRequest, $nameColumn)
    {
        if (request($keyRequest)){
            $this->query->orderBy($nameColumn, request($keyRequest));
        }
        return $this;
    }

    /**
     * first $keyRequest, second $nameColumn
     * Or if array key $keyRequest, value $nameColumn
     *
     * @param mixed ...$data
     * @return $this
     */
    public function filterOrderByIn(...$data)
    {
        $len = count($data);

        if($len > 2 && $len % 2 != 0){
            throw new InvalidArgumentException('There should be more than two values, and their sum should be even: ' . implode($data, ', '));
        }

        for ($i = 0; $i < count($data); $i+=2){
            if (!is_null(request($data[$i]))){
                $this->query->orderBy($data[$i+1], request($data[$i])?'asc':'desc');
            }
        }

        return $this;
    }

    public function filterSearch($keyRequest, ...$columns)
    {
        if(count($columns) < 0){
            throw new InvalidArgumentException('Second parameter is empty');
        }

        if (request($keyRequest)) {
            $value = request('s');
            $this->query->where(function ($q) use ($columns, $value){
                foreach ($columns as $column)
                    $q->where($column, 'like', "%$value%", 'or');
            });
        }

        return $this;
    }

}
