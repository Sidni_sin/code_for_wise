<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    CONST YES_AUTOLOAD = 1;
    CONST NOT_AUTOLOAD = 0;

    protected $_options = [];

    public function seed(int $store_id) : void
    {
        if(is_null($this->whereStoreId($store_id)->first())){
            $this->insertAll($this->parseOptions($this->getBaseOptionsStore()), $store_id);
        }else{
            throw new \LogicException('The function `(new Option())->seed()` must be run only once. In the table `options`, there is already a store_id with a value of ' . $store_id);
        }
    }


    public function getBaseOptionsStore() : array
    {
        return $options = [
            'meta_main_image'       => null,
            'meta_main_title'       => null,
            'meta_main_description' => null,
            'meta_main_keywords'    => null,
            'meta_main_locale'      => null,
            'meta_main_site'        => null,
            'lang_login'            => 'Вход',
            'lang_register'         => 'Регистер',
            'lang_main'             => 'Главная',
            'lang_category'         => 'Главные категории',
            'lang_page'             => 'Страницы',
            'social_page_google'            => 1,
            'social_page_instagram'         => 2,
            'social_page_twitter'           => 3,
            'social_page_facebook'          => 4,
            'social_page_vk'                => 5,
            'footer_columns_1_title'        => 'О нас',
            'footer_columns_1_text'         => 'Мы что-то тут это да 5 утра',
            'footer_columns_2_title'        => 'Наши контакты',
            'footer_columns_2_text'         => 'Ну это вообщем напишите нгомер на заборе',
            'button_show_all'               => 'Показать все',
        ];
    }

    private function parseOptions(array $data)
    {
        $options = [];
        foreach ($data as $key => $value){
            $options[]= ['name' => $key, 'value' => $value];
        }
        return $options;
    }

    public function insertAll(array $items, $store_id)
    {
        $now = \Carbon\Carbon::now();
        $items = collect($items)->map(function (array $data) use ($now, $store_id) {
            $data = array_merge(['store_id' => $store_id], $data);

            return $this->timestamps ? array_merge([
                'created_at' => $now,
                'updated_at' => $now,
            ], $data) : $data;
        })->all();

        return \DB::table($this->getTable())->insert($items);
    }

}