<?php

namespace App\Models;

use App\Observers\StoreIdObserver;
use App\Facades\SubDomainService;
use Illuminate\Support\Facades\File;

class Slider extends BaseModel
{
    use \App\Traits\Image;

    protected $fillable = [
        'store_id', 'title', 'text', 'price', 'src', 'url_button', 'text_type', 'position'
    ];

    public function getFolder()
    {
        return 'sliders';
    }
}
