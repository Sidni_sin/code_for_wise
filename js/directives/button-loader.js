let elClass = 'button-loader-directive';
let loader = '<span class="'+elClass+'"><i class="fas fa-cog fa-spin"></i></span> ';

export default {
    bind(el, bindings, vnode){
        setData(el, bindings.value);
    },
    update(el, bindings, vnode, oldVnode){
        setData(el, bindings.value);
    },
}
/*
*
* @param active{boolean}
* @return void
*
* */
function setData(el, active){
    if(active){
       el.innerHTML = loader+el.innerHTML;
    }else{
        let elem = el.getElementsByClassName(elClass);
        if(elem.length > 0){
            elem[0].remove();
        }
    }
}
